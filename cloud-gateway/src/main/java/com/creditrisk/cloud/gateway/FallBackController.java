package com.creditrisk.cloud.gateway;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallBackController {

	@GetMapping("/proposal-service-fallback")
	public String proposalServiceFallBackMethod() {
		return "Proposal service is taking longer than usual.Please try again";
	}
	
	@GetMapping("/risk-analysis-service-fallBack")
	public String riskAnalysisServiceFallBackMethod() {
		return "Risk Analysis service is taking longer than usual.Please try again";
	}
}
